Lab5 - https://www.digicert.com/resources/beginners-guide-to-tls-ssl-certificates-whitepaper-en-2019.pdf
Lab4 - USing Spring boot 
    Spring initialzier to Day3.Demo3.bootsec, add dependency - Web, Security
    copy pom and application
    create demo.MyController
        package demo;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

        @RestController
        @RequestMapping("/")
        public class MyController {
            @GetMapping
            public String index() {
                System.out.println("in index");
                return "<h1>IndexPage</h1><h2><a href='simple'>Simple</a></h2>" +
                "<h2><a href='admin/m1'>Admin Method1</a></h2>" + 
                "<h2><a href='admin/m2'>Admin Method2</a></h2>" ;
            }
            @GetMapping(value = "simple")
            public String simple() {
                System.out.println("in simple");
                return "<h1>Simple Page</h1><h2><a href='/'>home</a></h2>";
            }
            @GetMapping(value="admin/m1")
            public String method1() {
                System.out.println("in Method1");
                return "<h1>Admin Method1 Page</h1><a href='/'>home</a></h2>";
            }
            @GetMapping(value="admin/m2")
            public String method2() {
                System.out.println("in Method2");
                return "<h1>Admin Method2 Page</h1><a href='/'>home</a></h2>";
            }	
        }
    create demo.WebSecurityConfig
        package demo;

        @Configuration
        @EnableWebSecurity
        public class WebSecurityConfig {

            @Bean
            public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
                http
                    .authorizeHttpRequests((requests) -> 
                    requests.antMatchers("/", "/simple").permitAll()
                        .anyRequest().authenticated()
                    )
                    .httpBasic();
                    return http.build();
            }

            @Bean
            public UserDetailsService userDetailsService() {
                UserDetails user =
                    User.withDefaultPasswordEncoder()
                        .username("user1")
                        .password("abc")
                        .roles("USER")
                        .build();

                return new InMemoryUserDetailsManager(user);
            }
        }

Lab3 - enable web security
        Create maven project -> Day3.demo2.web , packaging- war 
        modify pom.xml to include
            <!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-webmvc</artifactId>
                <version>5.3.26</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.security</groupId>
                <artifactId>spring-security-config</artifactId>
                <version>5.8.2</version>
            </dependency>
            <!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-web -->
            <dependency>
                <groupId>org.springframework.security</groupId>
                <artifactId>spring-security-web</artifactId>
                <version>5.8.2</version>
            </dependency>
        create WEB-INF/web.xml
            <?xml version="1.0" encoding="UTF-8"?>
            <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns="http://xmlns.jcp.org/xml/ns/javaee"
                xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
                id="WebApp_ID" version="4.0">
                <display-name>tmp</display-name>
                <welcome-file-list>
                    <welcome-file>index.html</welcome-file>
                </welcome-file-list>
                <servlet>
                    <servlet-name>demo</servlet-name>
                    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
                    <load-on-startup>1</load-on-startup>
                </servlet>
                <servlet-mapping>
                    <servlet-name>demo</servlet-name>
                    <url-pattern>/app/*</url-pattern>
                </servlet-mapping>
                
                <context-param>
                    <param-name>contextConfigLocation</param-name>
                    <param-value>
                        /WEB-INF/demo-servlet.xml
                    </param-value>
                </context-param>


                <filter>
                    <filter-name>springSecurityFilterChain</filter-name>
                    <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
                </filter>
                <filter-mapping>
                    <filter-name>springSecurityFilterChain</filter-name>
                    <url-pattern>/*</url-pattern>
                </filter-mapping>
            <listener>
                    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
                </listener>


            </web-app>

        create demo-servlet.xml
            <?xml version="1.0" encoding="UTF-8"?>

            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:sec="http://www.springframework.org/schema/security"
                xmlns:mvc="http://www.springframework.org/schema/mvc"
                xmlns:context="http://www.springframework.org/schema/context"
                xsi:schemaLocation="http://www.springframework.org/schema/beans 
                    http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
                    http://www.springframework.org/schema/context
                    http://www.springframework.org/schema/context/spring-context-3.0.xsd
                    http://www.springframework.org/schema/security
                    https://www.springframework.org/schema/security/spring-security.xsd
                http://www.springframework.org/schema/mvc
                    http://www.springframework.org/schema/mvc/spring-mvc.xsd">


                <context:component-scan base-package="demo" />
                <mvc:annotation-driven></mvc:annotation-driven>
                <sec:authentication-manager>
                    <sec:authentication-provider>
                        <sec:user-service>
                            <sec:user name="user1" password="{noop}abc"
                                authorities="ROLE_admin" />
                            <sec:user name="user2" password="{noop}abc"
                                authorities="ROLE_stduser" />
                            <sec:user name="user3" password="{noop}abc"
                                authorities="ROLE_admin" />
                        </sec:user-service>
                    </sec:authentication-provider>
                </sec:authentication-manager>

                <sec:http auto-config="true">
                    <sec:intercept-url pattern="/app/admin/*"
                        access="hasRole('ROLE_admin')" />
                    <sec:http-basic />
                    <!-- <sec:form-login login-page="/login" default-target-url="/welcome" 
                        authentication-failure-url="/login?error" /> <sec:logout logout-success-url="/login?logout" 
                        /> -->
                </sec:http>
            </beans>


        create demo.MyController
            package demo;
            import org.springframework.web.bind.annotation.GetMapping;
            import org.springframework.web.bind.annotation.RequestMapping;
            import org.springframework.web.bind.annotation.RestController;

            @RestController
            @RequestMapping("/")
            public class MyController {
                @GetMapping
                public String index() {
                    System.out.println("in index");
                    return "<h1>IndexPage</h1><h2><a href='simple'>Simple</a></h2>" +
                    "<h2><a href='admin/m1'>Admin Method1</a></h2>" + 
                    "<h2><a href='admin/m2'>Admin Method2</a></h2>" ;
                }
                @GetMapping(value = "simple")
                public String simple() {
                    System.out.println("in simple");
                    return "<h1>Simple Page</h1><h2><a href='/Day3.demo2.web/app/'>home</a></h2>";
                }
                @GetMapping(value="admin/m1")
                public String method1() {
                    System.out.println("in Method1");
                    return "<h1>Admin Method1 Page</h1><a href='/Day3.demo2.web/app/'>home</a></h2>";
                }
                @GetMapping(value="admin/m2")
                public String method2() {
                    System.out.println("in Method2");
                    return "<h1>Admin Method2 Page</h1><a href='/Day3.demo2.web/app/'>home</a></h2>";
                }	
            }
        run 
            observe -> DelegatingFilterProxy, Filters
            




Lab2 - Modify demo.xml to just declare bean without security inteceptors
        Modify simple.java methods with Secured annotation

            @Secured(value = "ROLE_stduser")
	        public void method1() {

            	@Secured(value = "ROLE_admin")
	        public void method2() {
        run 

Lab1 - Maven Project (Day3.demo1)
    Modify pom file to include 
        <dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>5.3.26</version>
		</dependency>
    Write demo.xml in resources
        <?xml version="1.0" encoding="UTF-8"?>
        <beans xmlns="http://www.springframework.org/schema/beans"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.springframework.org/schema/beans
                https://www.springframework.org/schema/beans/spring-beans.xsd">
                
                <bean id="simple" class="demo.Simple" scope="singleton"  />
        </beans>      
    Write demo.Simple
        package demo;
        public class Simple {
            public Simple() {
                System.out.println("Simple Constructor invoked");
            }
            
            public void method1() {
                System.out.println("Simple - method1 invoked ");
            }
            public void method2() {
                System.out.println("Simple - method2 invoked ");
            }
        }
    Write demo.SecurityClient
        package demo;

        import org.springframework.context.ApplicationContext;
        import org.springframework.context.support.ClassPathXmlApplicationContext;
        import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
        import org.springframework.security.core.Authentication;
        import org.springframework.security.core.context.SecurityContextHolder;
        import org.springframework.security.core.context.SecurityContextImpl;

        public class SecurityClient {

            public static void main(String[] args) {
                // TODO Auto-generated method stub
                ApplicationContext context  = new ClassPathXmlApplicationContext("demo.xml");
                System.out.println("-------Contenxt Loaded--------");
               
                Simple simple  = context.getBean("simple", Simple.class);
                try {
                simple.method1();
                }catch(Exception e) {
                    System.out.println("Exception calling Method1 " + e);
                }
                try {
                    simple.method2();
                    }catch(Exception e) {
                        System.out.println("Exception calling Method2 " + e);
                    }
            }
        }
    run -> No security
    Enable Security
        Modify pom file to include 
            <dependency>
                <groupId>org.springframework.security</groupId>
                <artifactId>spring-security-config</artifactId>
                <version>5.8.2</version>
            </dependency>
        Modify demo.xml
            <?xml version="1.0" encoding="UTF-8"?>
            <beans xmlns="http://www.springframework.org/schema/beans"
                xmlns:sec="http://www.springframework.org/schema/security"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.springframework.org/schema/beans
                    https://www.springframework.org/schema/beans/spring-beans.xsd
                    http://www.springframework.org/schema/security
                    https://www.springframework.org/schema/security/spring-security.xsd">

                <bean id="simple" class="demo.Simple">
                    <sec:intercept-methods>
                        <sec:protect access="ROLE_admin" method="method2" />
                        <sec:protect access="ROLE_stduser" method="method1" />
                    </sec:intercept-methods>

                </bean>
                <sec:authentication-manager>
                    <sec:authentication-provider>
                        <sec:user-service>
                            <sec:user name="user1" password="{noop}abc"
                                authorities="ROLE_admin" />
                            <sec:user name="user2" password="{noop}abc"
                                authorities="ROLE_stduser" />
                            <sec:user name="user3" password="{noop}abc"
                                authorities="ROLE_admin" />
                        </sec:user-service>
                    </sec:authentication-provider>
                </sec:authentication-manager>

                <sec:global-method-security
                    secured-annotations="enabled" />
            </beans>      
    run -> error -> An Authentication object was not found in the SecurityContext
    Modify SecurityClient to include these lines before ctx.getbean call
            SecurityContextImpl scimpl = new SecurityContextImpl();
            
            Authentication auth = new UsernamePasswordAuthenticationToken("user", "abc");
            
            scimpl.setAuthentication(auth);
            SecurityContextHolder.setContext(scimpl);
    run ->Bad credentials
    Modify user to user1
        method1 - Access is denied, method2 - allowed
    Modify user1 to user2
        method1 - allowed, method2 - Access is denied